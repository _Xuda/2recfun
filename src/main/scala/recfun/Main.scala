package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
    * Exercise 1
    */
    def pascal(c: Int, r: Int): Int = {
        var counter = 0
        if(c==0 || r==c) counter = 1
        else counter = pascal(c-1,r-1) + pascal(c,r-1)
        counter
      }
  /**
    * Exercise 2
    */

  def balance(chars: List[Char]): Boolean = {
    def helper(subChars: List[Char], count: Int): Boolean = {
      if (subChars.isEmpty) count==0
      else if (subChars.head==')') {
        if (count<1) false
        else helper(subChars.tail, count-1)
      }
      else if (subChars.head=='(') helper(subChars.tail, count+1)
      else helper(subChars.tail, count)
    }
    helper(chars, 0)
  }

  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]): Int = {
    if (money == 0)
      1
    else if (money > 0 && coins.nonEmpty)
      countChange(money - coins.head, coins) + countChange(money, coins.tail)
    else
      0
  }

}
